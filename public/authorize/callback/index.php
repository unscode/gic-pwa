<?php
error_reporting(false);

// Define a URL de destino.
$url = 'http://gic.unscode.local/oauth/token';

// Cria nosso array de dados para enviar pela requisição.
$data = [
    'grant_type' => 'authorization_code',
    'client_id' => '1',
    'client_secret' => 'f90wcwro3CcX56Jc32ELqEEbRnCRsivNJ1BBTa2I',
    'redirect_uri' => 'http://front.unscode.local/authorize/callback',
    'code' => $_GET['code'],
];

// Define o array de opções do CURL.
$options = [
    CURLOPT_URL => $url,
    CURLOPT_POST => true,
    CURLOPT_POSTFIELDS => $data,
    CURLOPT_RETURNTRANSFER => true,
];

// Inicia o objeto cURL.
$curl = curl_init();

// Atribui as opçãoes..
curl_setopt_array($curl, $options);

// Executa a requisição POST.
$results = curl_exec($curl);

// Fecha a sessão.
curl_close($curl);

// Converte a resposta para um array associativo.
$arr = json_decode($results, true);

setcookie("at", $arr['access_token'], time() + $arr['expires_in'], "/", ".front.unscode.local");
header("Location:/");
