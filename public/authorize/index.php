<?php

// Gera a string de consulta codificada por URL.
$query = http_build_query([
    'client_id' => '1',
    'redirect_uri' => 'http://front.unscode.local/authorize/callback',
    'response_type' => 'code',
    'scope' => '',
]);

// Envia um cabeçalho HTTP não processado
header("Location:http://gic.unscode.local/oauth/authorize?" . $query);
