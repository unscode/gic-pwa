import Vue from 'vue';
import VueCookies from 'vue-cookies';
import VueLodash from 'vue-lodash';
import VueTheMask from 'vue-the-mask';

/* ------------------------------------------------------------------------------ */
import Buefy from 'buefy';
import Axios from 'axios';
/* ------------------------------------------------------------------------------ */
import App from './App.vue';
import router from './router';
import store from './store';
/* ------------------------------------------------------------------------------ */
import './registerServiceWorker';

// Configuração do VueJs
Vue.prototype.$http = Axios;
Vue.config.productionTip = false;

Vue.use(VueCookies);
Vue.use(VueLodash);
Vue.use(VueTheMask);
Vue.use(Buefy);

const app = new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app');

// Configurações do Axios
Axios.defaults.baseURL = 'http://gic.unscode.local/api/';
Axios.defaults.headers.common['Access-Control-Allow-Origin'] = '*';
Axios.defaults.headers.common.Authorization = `Bearer ${VueCookies.get('at')}`;
