import Vue from 'vue';
import VueCookies from 'vue-cookies';
import Router from 'vue-router';
import Welcome from './components/Welcome';

Vue.use(VueCookies);
Vue.use(Router);

const ifAuthenticated = (to, from, next) => {
    if (VueCookies.get('at')) {
        next();
        return;
    }
    next('/login');
};

const ifNotAuthenticated = (to, from, next) => {
    if (!store.getters.isAuthenticated) {
        next();
        return;
    }
    next('/');
};

const router = new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: [
        {
            path: '/',
            name: 'home',
            component: Welcome,
            beforeEnter: ifAuthenticated,
        },
        {
            path: '/game',
            name: 'game.index',
            component: () => import('./views/game/GameIndex.vue'),
            beforeEnter: ifAuthenticated,
        },
        {
            path: '/game/:game',
            name: 'game.show',
            component: () => import('./views/game/GameShow.vue'),
            beforeEnter: ifAuthenticated,
        },
        {
            path: '/game/:game/phase/:phase',
            name: 'game.phase.show',
            component: () => import('./views/game/phase/PhaseShow.vue'),
            beforeEnter: ifAuthenticated,
        },
        {
            path: '/medal',
            name: 'medal.index',
            component: () => import('./views/medal/MedalIndex.vue'),
            beforeEnter: ifAuthenticated,
        },
        {
            path: '/medal/:medal',
            name: 'medal.show',
            component: () => import('./views/medal/MedalShow.vue'),
            beforeEnter: ifAuthenticated,
        },
        {
            path: '/about',
            name: 'about',
            component: () => import('./components/Bit.vue'),
        },
        {
            path: '/404',
            name: '404',
            component: () => import('./views/errors/PageNotFound.vue'),
        },
        {
            path: '/login',
            name: 'login',
            component: () => import('./views/auth/Login.vue'),
            // beforeEnter: ifNotAuthenticated,
        },
    ],
});

export default router;
